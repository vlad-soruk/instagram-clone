import React, { useState } from "react";
import FollowButton from "../FollowButton/FollowButton";

import styles from "./Person.module.scss";

const Person = ({ person, onToggleSubscription, isFollowing, buttonHidden }) => {
  return (
    <div class={styles.personContainer}>
      <div key={person.id} className={styles.person}>
        <div class={styles.personAvatarAndNameContainer}>
          <img src={person.icon} alt={person.username} />
          <span>{person.username}</span>
        </div>
        {buttonHidden 
          ? null
          : <FollowButton 
              isFollowing={isFollowing}
              onClick={() => onToggleSubscription(person.id)}/>
          // : <button 
          //     className={ `${styles.followButton} ${isFollowing ? styles.unsubscribeButton : ""} `}
          //     onClick={() => onToggleSubscription(person.id)}>
          //       {isFollowing ? "Відписатися" : "Стежити"}
          //   </button>
        }
      </div>
    </div>
  );
};

export default Person;
