import React, { useState } from "react";
import Person from "../Person/Person";

import styles from "./PeopleToFollow.module.scss";

const PeopleToFollow = ({
  peopleToFollow,
  subscribedPeople,
  setSubscribedPeople,
}) => {
  const handleToggleSubscription = (personId) => {
    const personToFollow = peopleToFollow.find(
      (person) => person.id === personId
    );

    if (personToFollow) {
      if (
        subscribedPeople.some(
          (subscribedPerson) => subscribedPerson.id === personId
        )
      ) {
        setSubscribedPeople((prevSubscribedPeople) =>
          prevSubscribedPeople.filter((person) => person.id !== personId)
        );
      } else {
        setSubscribedPeople((prevSubscribedPeople) => [
          ...prevSubscribedPeople,
          personToFollow,
        ]);
      }
    }
  };

  return (
    <div className={styles.peopleToFollowSection}>
      <h1 className={styles.peopleToFollowTitle}>Рекомендації для Вас</h1>
      <div className="people-to-follow">
        {peopleToFollow
          .filter(
            (person) =>
              !subscribedPeople.some(
                (subscribedPerson) => subscribedPerson.id === person.id
              )
          )
          .map((person) => (
            <div className={styles.peopleToFollowContainer}>
              <Person
                key={person.id}
                person={person}
                onToggleSubscription={handleToggleSubscription}
              />
            </div>
          ))}
      </div>
    </div>
  );
};

export default PeopleToFollow;
