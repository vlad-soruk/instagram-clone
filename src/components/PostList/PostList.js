
import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";


import styles from "./PostList.module.scss";

const PostList = ({
  posts,
  onLike,
  onComment,
  onToggleComments,
  loadMorePosts,
  loadedPosts
}) => {
  useEffect(()=>{
    setTimeout(()=>{
      console.log(posts);
    },5000)
  }, [])

  const [showAllComments, setShowAllComments] = useState({});
  const [newComment, setNewComment] = useState("");
  const [toggledComments, setToggledComments] = useState(false);

  const toggleAllComments = (postId) => {
    setShowAllComments((prevState) => ({
      ...prevState,
      [postId]: !prevState[postId],
    }));

    setToggledComments(prevstate => !prevstate)
  };

  const toggleSingleComment = (postId, index) => {
    setShowAllComments((prevState) => ({
      ...prevState,
      [postId]: { ...prevState[postId], [index]: !prevState[postId]?.[index] },
    }));
  };

  const handleCommentChange = (event) => {
    setNewComment(event.target.value);
  };

  const handleCommentSubmit = (postId) => {
    if (newComment) {
      onComment(postId, newComment);
      setNewComment("");
    }
  };

  return (
    <>
      <h1 className={styles.postListTitle}>Стрічка з постів</h1>
      <InfiniteScroll

        dataLength={posts.length}
        next={loadMorePosts}
        hasMore={true}
        loader={<h4>Loading...</h4>}
      >
        {posts.map((post) => (
          <div key={post.id} className="post">
            <div class="postImageContainer">
              <img
                src={post.image}
                alt={`Post ${post.id}`}
                onDoubleClick={() => {
                  onLike(post.id)
                  console.log("POSTS in PostList comp: ", posts);
                  console.log("sliced POSTS in PostList comp: ", posts.slice(0, undefined));
                  console.log("Loaded Posts Number: ", loadedPosts);
                }}
                className={post.userLiked ? "liked" : ""}
              />
            </div>
            
            <div class={styles.buttonsAndInputsContainer}>
              <button className={styles.likeButton} onClick={() => onLike(post.id)}>
                {/* {post.userLiked ? "❤️" : "🤍"} */}
                {post.userLiked
                ? <svg className={styles.svgAlreadyLiked} aria-label="Не нравится" color="rgb(255, 48, 64)" fill="rgb(255, 48, 64)" height="24" role="img" viewBox="0 0 48 48" width="24"><title>Не нравится</title><path d="M34.6 3.1c-4.5 0-7.9 1.8-10.6 5.6-2.7-3.7-6.1-5.5-10.6-5.5C6 3.1 0 9.6 0 17.6c0 7.3 5.4 12 10.6 16.5.6.5 1.3 1.1 1.9 1.7l2.3 2c4.4 3.9 6.6 5.9 7.6 6.5.5.3 1.1.5 1.6.5s1.1-.2 1.6-.5c1-.6 2.8-2.2 7.8-6.8l2-1.8c.7-.6 1.3-1.2 2-1.7C42.7 29.6 48 25 48 17.6c0-8-6-14.5-13.4-14.5z"></path></svg>
                :  <svg className={styles.svgNotLikedYet} aria-label="Нравится" color="rgb(38, 38, 38)" fill="rgb(38, 38, 38)" height="24" role="img" viewBox="0 0 24 24" width="24"><title>Нравится</title><path d="M16.792 3.904A4.989 4.989 0 0 1 21.5 9.122c0 3.072-2.652 4.959-5.197 7.222-2.512 2.243-3.865 3.469-4.303 3.752-.477-.309-2.143-1.823-4.303-3.752C5.141 14.072 2.5 12.167 2.5 9.122a4.989 4.989 0 0 1 4.708-5.218 4.21 4.21 0 0 1 3.675 1.941c.84 1.175.98 1.763 1.12 1.763s.278-.588 1.11-1.766a4.17 4.17 0 0 1 3.679-1.938m0-2a6.04 6.04 0 0 0-4.797 2.127 6.052 6.052 0 0 0-4.787-2.127A6.985 6.985 0 0 0 .5 9.122c0 3.61 2.55 5.827 5.015 7.97.283.246.569.494.853.747l1.027.918a44.998 44.998 0 0 0 3.518 3.018 2 2 0 0 0 2.174 0 45.263 45.263 0 0 0 3.626-3.115l.922-.824c.293-.26.59-.519.885-.774 2.334-2.025 4.98-4.32 4.98-7.94a6.985 6.985 0 0 0-6.708-7.218Z"></path></svg>
                }
                </button>
              <input
                className={styles.inputAddComment}
                type="text"
                placeholder="Додати коментар..."
                value={newComment}
                onChange={handleCommentChange}
                onKeyDown={(event) => {
                  if (event.key === "Enter") {
                    handleCommentSubmit(post.id);
                  }
                }}
              />
              <button className={styles.sendMsgBtn} onClick={() => handleCommentSubmit(post.id)}>
                Відправити
              </button>
            </div>

            {post.comments.length > 0 && (
              <div className="comments-section">
                {showAllComments[post.id] ? (
                  post.comments.map((comment, index) => (
                    <div key={index} className="comment">
                      {comment}
                    </div>
                  ))
                ) : (
                  <div className="comment">
                    {post.comments[post.comments.length - 1]}
                  </div>
                )}
                {post.comments.length > 1 && (
                  <button className={styles.toggleCommentsBtn} onClick={() => toggleAllComments(post.id)}>
                    {toggledComments ? "Скрыть комментарии" : "Показать больше комментариев"}
                  </button>
                )}
              </div>
            )}
          </div>
        ))}
      </InfiniteScroll>
    </>
  );
};

export default PostList;
