import React from "react";

import styles from "./FollowButton.module.scss"

const FollowButton = ({ isFollowing, onClick }) => {
  return (
    <button className={ `${styles.followButton} ${isFollowing ? styles.unsubscribeButton : ""} `} onClick={onClick}>
      {isFollowing ? "Відписатись" : "Стежити"}
    </button>
  );
};

export default FollowButton;
