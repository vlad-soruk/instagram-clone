import React, { useState, useEffect, createContext } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import SubscribedPeople from "../SubscribedPeople/SubscribedPeople";
import PeopleToFollow from "../PeopleToFollow/PeopleToFollow";
import PostList from "../PostList/PostList";
import PostListContainer from "../PostListContainer/PostListContainer";

import "./App.scss";

export const NumberContext = createContext(null)

function App() {
  const [posts, setPosts] = useState([]);
  const [commentsVisible, setCommentsVisible] = useState({});
  const [subscribedPeople, setSubscribedPeople] = useState([]);
  const [peopleToFollow, setPeopleToFollow] = useState([]);

  const [loadedPosts, setLoadedPosts] = useState(3);

  useEffect(() => {
    fetch("/posts.json")
      .then((response) => response.json())
      .then((data) => setPosts(data));

    fetch("/subscribedPeople.json")
      .then((response) => response.json())
      .then((data) => setSubscribedPeople(data));

    fetch("/peopleToFollow.json")
      .then((response) => response.json())
      .then((data) => setPeopleToFollow(data));
  }, []);

  const loadMorePosts = () => {
    fetch("/posts.json")
      .then((response) => response.json())
      .then((allPosts) => {
        const numberOfPostsToLoad = 3;

        const newPosts = allPosts.slice(
          loadedPosts,
          loadedPosts + numberOfPostsToLoad
        );

        setPosts((prevPosts) => [...prevPosts, ...newPosts]);

        setLoadedPosts(loadedPosts + newPosts.length);
      })
      .catch((error) => {
        console.error(
          "Произошла ошибка при загрузке дополнительных постов:",
          error
        );
      });
  };

  const handleLike = (postId) => {
    setPosts((prevPosts) =>
      prevPosts.map((post) =>
        post.id === postId
          ? {
              ...post,
              userLiked: !post.userLiked,
              likes: post.userLiked ? post.likes - 1 : post.likes + 1,
            }
          : post
      )
    );
  };

  const handleComment = (postId, comment) => {
    console.log(comment);
    setPosts((prevPosts) =>
      prevPosts.map((post) =>
        post.id === postId
          ? {
              ...post,
              comments: [...post.comments, comment],
            }
          : post
      )
    );
  };

  const handleToggleComments = (postId) => {
    setCommentsVisible((prevState) => ({
      ...prevState,
      [postId]: !prevState[postId],
    }));
  };

  const handleToggleSubscription = (personId) => {
    setPeopleToFollow((prevPeople) =>
      prevPeople.map((person) =>
        person.id === personId
          ? {
              ...person,
              isFollowing: !person.isFollowing,
            }
          : person
      )
    );

    const personToFollow = peopleToFollow.find(
      (person) => person.id === personId
    );

    if (personToFollow.isFollowing) {
      setSubscribedPeople((prevSubscribedPeople) => [
        ...prevSubscribedPeople,
        personToFollow,
      ]);
    } else {
      setSubscribedPeople((prevSubscribedPeople) =>
        prevSubscribedPeople.filter((person) => person.id !== personId)
      );
    }
  };

  return (
    <NumberContext.Provider value={loadedPosts}>
      <Router>
        <div className="App">
          <Routes>
            <Route
              path="/"
              element={
                <SubscribedPeople
                  posts={posts}
                  commentsVisible={commentsVisible}
                  subscribedPeople={subscribedPeople}
                  handleLike={handleLike}
                  handleComment={handleComment}
                  handleToggleComments={handleToggleComments}
                  handleToggleSubscription={handleToggleSubscription}
                />
              }
            />
            <Route
              path="/posts/:userId"
              element={
                <PostListContainer
                  posts={posts}
                  commentsVisible={commentsVisible}
                  handleLike={handleLike}
                  handleComment={handleComment}
                  handleToggleComments={handleToggleComments}
                  loadedPosts={loadedPosts}
                  loadMorePosts={loadMorePosts}
                />
              }
            />
          </Routes>

          <PeopleToFollow
            peopleToFollow={peopleToFollow}
            onFollowToggle={handleToggleSubscription}
            subscribedPeople={subscribedPeople}
            setPeopleToFollow={setPeopleToFollow}
            setSubscribedPeople={setSubscribedPeople}
          />
                  </div>
      </Router>
    </NumberContext.Provider>
  );
}

export default App;
