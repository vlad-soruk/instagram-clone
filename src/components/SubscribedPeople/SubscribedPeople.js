import React from "react";
import { Link } from "react-router-dom";
import PostListContainer from "../PostListContainer/PostListContainer";
import FollowButton from "../FollowButton/FollowButton";

import styles from "./SubscribedPeople.module.scss"
import Person from "../Person/Person";
import PeopleToFollow from "../PeopleToFollow/PeopleToFollow";

const SubscribedPeople = ({
  posts,
  commentsVisible,
  setCommentsVisible,
  subscribedPeople,
  handleLike,
  handleComment,
  handleToggleComments,
  handleToggleSubscription,
}) => {
  return (
    <div className={styles.subscribedPeopleSectionContainer}>
      <div class={styles.alreadySubscribedPeopleAndTitleContainer}>
        <h1 className={styles.subscribedPeopleTitle}>Список людей, на яких Ви підписані</h1>
        {subscribedPeople.map((person) => (
            <div className={styles.linkToSubscribedPersonContainer}>
              <Link className={styles.subscribedPersonLink} key={person.id} to={`/posts/${person.id}`}>
                <Person person={person} buttonHidden={true}/>
                {/* <div >
                  <img src={person.icon} alt={person.username} />
                  <span>{person.username}</span>
                </div> */}
              </Link>
                  {/* <FollowButton
                    isFollowing={true}
                    onClick={() => handleToggleSubscription(person.id)}
                  /> */}

              <FollowButton
                  isFollowing={true}
                  onClick={() => handleToggleSubscription(person.id)}
              />
            </div>
          ))}
      </div>
      <div>
        <PostListContainer
          posts={posts}
          commentsVisible={commentsVisible}
          setCommentsVisible={setCommentsVisible}
          handleLike={handleLike}
          handleComment={handleComment}
          handleToggleComments={handleToggleComments}
        />
      </div>
    </div>
  );
};

export default SubscribedPeople;
