import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import PostList from "../PostList/PostList";

import "./PostListContainer.scss";
import { NumberContext } from "../App/App";

const PostListContainer = ({
  posts,
  commentsVisible,

  handleLike,
  handleComment,
  handleToggleComments,
  loadedPosts,
  loadMorePosts,
}) => {
  const loadedPostsNumber = useContext(NumberContext)

  const { userId } = useParams();
  const dataArr = userId
    ? posts.filter((post) => +userId === post.userId)
    : posts;
  
  return (
    <PostList
      loadedPosts={loadedPosts}
      posts={posts.slice(0, loadedPostsNumber)}
      // posts={dataArr.slice(0, loadedPosts)}
      commentsVisible={commentsVisible}
      onLike={handleLike}
      onComment={handleComment}
      onToggleComments={handleToggleComments}
      loadMorePosts={loadMorePosts}
    />
  );
};

export default PostListContainer;
